#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QDebug>
#include <QClipboard>
#include <QKeyEvent>
#include <QMessageBox>

#include <QDebug>


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    model(new TableModel())
{
    ui->setupUi(this);
    _editorButton = new QPushButton(ui->centralWidget);

    ui->tableView->setModel(model);

    //
    _editorButton->setObjectName("EditorButton");
    _editorButton->setCheckable(true);
    _editorButton->setGeometry(0, 0, 6, 6);
    _editorButton->setAttribute(Qt::WA_TranslucentBackground);
    _editorButton->setStyleSheet("#EditorButton { border-radius: 0px; }");
    _editorButton->setFocusPolicy(Qt::NoFocus);
    _editorButton->show();
    setEditor(false);

    //
    connect(ui->tableView->verticalHeader(), SIGNAL(geometriesChanged()),
            this, SLOT(resizeEditorButtonGeometry()));
    connect(ui->tableView->horizontalHeader(), SIGNAL(geometriesChanged()),
            this, SLOT(resizeEditorButtonGeometry()));
    connect(_editorButton, SIGNAL(toggled(bool)),
            this, SLOT(setEditor(bool)));
}

MainWindow::~MainWindow()
{
    delete _editorButton;
    delete ui;
}

//------------------------------------------------------------------------------

void MainWindow::keyPressEvent(QKeyEvent *event)
{
    if (event->matches(QKeySequence::Copy) ||
            event->matches(QKeySequence::Cut)) {
        copyTableToClipboard();
    }
    else if (event->matches(QKeySequence::Paste)) {
        pasteClipboardToTable();
    }
    else
        QWidget::keyPressEvent(event);
}

//------------------------------------------------------------------------------

void MainWindow::resizeEditorButtonGeometry()
{
    QRect where = QRect(
                ui->tableView->pos(),
                QSize(
                    ui->tableView->verticalHeader()->size().width() +1,
                    ui->tableView->horizontalHeader()->size().height() +1));
    _editorButton->setGeometry(where);
}

void MainWindow::setEditor(bool enable)
{
    if (enable) {
        setWindowTitle("Application - [EDIT]");
        _editorButton->setToolTip("Save modification of the table.");
        _editorButton->setIcon(QIcon::fromTheme("document-save"));
        ui->tableView->setEditTriggers(QAbstractItemView::DoubleClicked |
                                       QAbstractItemView::SelectedClicked |
                                       QAbstractItemView::EditKeyPressed);
    }
    else {
        setWindowTitle("Application");
        _editorButton->setToolTip("Edit the table.");
        _editorButton->setIcon(QIcon::fromTheme("document-properties"));
        ui->tableView->setEditTriggers(QAbstractItemView::NoEditTriggers);
    }
}

//------------------------------------------------------------------------------

QRect MainWindow::selectedRect() const
{
    QRect rect;
    // on recupere la liste en selection
    QModelIndexList listSelect = ui->tableView->selectionModel()->selectedIndexes();

    // si vide
    if (listSelect.size() <= 0) {
        rect = QRect(-1, -1, 0, 0); // hack pour signifier vide (sinon null)
    }
    // si exactement un element selectionné
    else if (listSelect.size() == 1) {
        rect = QRect(listSelect.first().column(), listSelect.first().row(), 1, 1);
    }
    else if (listSelect.size() > 1) {
        // on les trie (trie intellingent, par ligne croissant, puis par colonne croissant)
        qSort(listSelect.begin(), listSelect.end());

        const QModelIndex firstIndex = listSelect.takeFirst();
        const QModelIndex lastIndex = listSelect.takeLast();
        QModelIndex prevIndex = firstIndex;
        int nbColumn = lastIndex.column() - firstIndex.column() +1;
        int nbRow = lastIndex.row() - firstIndex.row() +1;

        bool isValid = (nbColumn*nbRow) == (listSelect.size()+2);

        if (isValid) {
            // on verifie que cela fait bien un rectangle
            Q_FOREACH(QModelIndex index, listSelect) {

                // on s'assure que l'index est dans les bornes
                if (index.column() < firstIndex.column() ||
                        index.column() > lastIndex.column() ||
                        index.row() < firstIndex.row() ||
                        index.row() > lastIndex.row()) {
                    isValid &= false;
                }

                // on s'assure qu'il suit bien le precedent

                // si changement de ligne
                if (index.row() != prevIndex.row()) {
                    isValid &= (prevIndex.column() == lastIndex.column()) &&
                                (index.row()-1 == prevIndex.row()) &&
                                (index.column() == firstIndex.column());
                }
                // si pas de changement de ligne
                else {
                    isValid &= (index.row() == prevIndex.row()) &&
                            (index.column()-1 == prevIndex.column());
                }

                // si erreur
                if (!isValid)
                    break;

                // tour suivant
                prevIndex = index;
            }
        }

        // si selection rectangulaire (si valide)
        if (isValid) {
            rect = QRect(firstIndex.column(), firstIndex.row(),
                         nbColumn, nbRow);
        }
    }
    return rect;
}

void MainWindow::copyTableToClipboard()
{
    // recuperation du rectangle selectionnee
    QRect rectSelection = selectedRect();

    // si erreur de selection OU vide
    if (rectSelection.isNull()) {
        // si vide (check de hack)
        if (rectSelection.top() < 0 || rectSelection.left() < 0) {
            rectSelection = QRect(0, 0, 0, 0);
        }
        // sinon erreur
        else {
            QMessageBox::warning(this, "Erreur de sélection",
                                 "Veuillez sélectionner une cellule ou un rectangle de cellules.",
                                 QMessageBox::Ok, QMessageBox::Ok);
            return;
        }
    }

    // On construit le resultat final
    QString builder;
    if (rectSelection.width() > 0 && rectSelection.height() > 0) {
        for (int row=rectSelection.top(); row <= rectSelection.bottom(); row++) {
            if (row != rectSelection.top())
                builder.append("\n");
            for (int column=rectSelection.left(); column <= rectSelection.right(); column++) {
                if (column != rectSelection.left())
                    builder.append("\t");
                builder.append(model->data(model->index(row, column), Qt::DisplayRole).toString());
            }
        }
    }

    qApp->clipboard()->setText(builder, QClipboard::Clipboard);
}

void MainWindow::pasteClipboardToTable()
{
    // recuperation du rectangle selectionnee
    QRect rectSelection = selectedRect();

    // si erreur de selection OU vide
    if (rectSelection.isNull()) {
        // si vide (check de hack)
        if (rectSelection.top() < 0 || rectSelection.left() < 0) {
            rectSelection = QRect(0, 0, 0, 0);
        }
        // sinon erreur
        else {
            QMessageBox::warning(this, "Erreur de sélection (pour collage)",
                                 "Veuillez sélectionner une cellule ou un rectangle de cellules.\n"
                                 "Cela définit la plage de cellules dans laquelle le collage se fera.\n"
                                 "Les données copiées et la plage sélectionnée doivent avoir la même dimension, ou alors veuillez ne sélectionner q'une seule cellule.",
                                 QMessageBox::Ok, QMessageBox::Ok);
            return;
        }
    }

    bool isMonoCellSelection = (rectSelection.width() == 1) &&
            (rectSelection.height() == 1);

    // récuperation du contenu depuis le press-papier
    QString cpContent = qApp->clipboard()->text(QClipboard::Clipboard);
    // suppression du dernier '\n' (car il peut exiter, et il ne sert à rien)
    cpContent = cpContent.trimmed();

    // comptage du nombre de ligne et colonne
    // et verification de la consistence des données (quelles soient bie
    // rectangulaire)
    QStringList cpSplitRow ;
    if (!cpContent.isEmpty())
        cpSplitRow = cpContent.split(QLatin1Char('\n'),
                                     QString::SkipEmptyParts);
    int nbRow = cpSplitRow.size();
    int nbColumn = -1;
    QStringList cpStringDatas;
    bool isValid = true;
    Q_FOREACH(QString cpRow, cpSplitRow) {
        QStringList splittedRow = cpRow.split(QLatin1Char('\t'),
                                              QString::SkipEmptyParts);
        // si nombre de colonne inconnue
        if (nbColumn < 0) {
            nbColumn = splittedRow.size();
        }
        // sinon si nombre de colonne connue
        else {
            // on verifie que c'est la bien la meme taille
            isValid &= (nbColumn == splittedRow.size());
        }
        // si erreur
        if (!isValid)
            break;
        // sinon  on continue
        // et on enregistre le resultat final pas a pas
        cpStringDatas.append(splittedRow);
    }

    // si erreur
    if (!isValid) {
        QMessageBox::warning(this, "Erreur de collage",
                             "Les données copiés ne sont pas bien formattées.\n"
                             "Collage impossible.",
                             QMessageBox::Ok, QMessageBox::Ok);
    }
    // si vide -> RAF
    else if (cpStringDatas.isEmpty()) {
        QMessageBox::information(this, "Erreur de collage",
                                 "Il n'y a rien à coller depuis le presse-papier.",
                                 QMessageBox::Ok, QMessageBox::Ok);
    }
    // si les dimensions des données et de la selection sont différentes
    // (dans le cas d'une selection d'un rectangle)
    else if (!isMonoCellSelection &&
             (nbColumn != rectSelection.width() ||
             nbRow != rectSelection.height())) {
        QMessageBox::warning(this, "Erreur de collage",
                             QString("Impossible de copiés un tableau de %1x%2 dans un rectangle de %3x%4.\n")
                             .arg(nbRow).arg(nbColumn).arg(rectSelection.height()).arg(rectSelection.width()),
                             QMessageBox::Ok, QMessageBox::Ok);
    }
    // si tout est ok
    else {
        // mise à jour du rectangle (pour ne pas tester isMonoCellSelection)
        rectSelection = QRect(rectSelection.topLeft(), QSize(nbColumn, nbRow));
        QModelIndexList listToSelect;

        int dataIndex = 0;
        for (int row = rectSelection.top(); row <= rectSelection.bottom(); row++) {
            for (int column=rectSelection.left(); column <= rectSelection.right(); column++) {
                QString data = cpStringDatas[dataIndex++];
                QModelIndex index = model->index(row, column);
                model->setData(index, data, Qt::DisplayRole);
                listToSelect << index;
            }
        }

        // selection de la zone collée (dans le cas ou isMonoCellSelection)
        ui->tableView->selectionModel()->clearSelection();
        Q_FOREACH(QModelIndex index, listToSelect) {
            ui->tableView->selectionModel()->select(index,
                                                    QItemSelectionModel::Select);
        }
    }

}

//==============================================================================

TableModel::TableModel(QObject* parent) :
    QAbstractTableModel(parent),
    values()
{
    //
    double timeCounter = 0.0;
    values << timeCounter++ << 47.0348 << 2.3697 << 100.9;
    values << timeCounter++ << 47.0399 << 2.3713 << 101.2;
    values << timeCounter++ << 47.0427 << 2.3781 << 101.0;
    values << timeCounter++ << 47.0503 << 2.3820 << 101.4;
    values << timeCounter++ << 47.0689 << 2.3790 << 100.7;
    values << timeCounter++ << 47.0820 << 2.3735 << 100.9;
    values << timeCounter++ << 47.0864 << 2.3709 << 101.0;
    values << timeCounter++ << 47.0932 << 2.3670 << 101.6;
}

QVariant TableModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    QVariant res;

    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole ||
                role == Qt::EditRole) {
            if (section == 0)
                res = "Datation";
            else if (section == 1)
                res = "Latitude";
            else if (section == 2)
                res = "Longitude";
            else if (section == 3)
                res = "Altitude";
        }
    }
    else if (orientation == Qt::Vertical) {
        if (role == Qt::DisplayRole ||
                role == Qt::EditRole) {
            res = "00" + QString::number(section);
        }
    }

    return res;
}

QVariant TableModel::data(const QModelIndex & index, int role) const
{
    QVariant res;

    if (role == Qt::DisplayRole ||
            role == Qt::EditRole) {
        int prec = index.column() == 0
                ? 1
                : index.column() == 3
                  ? 2
                  : 3;
        res = QString::number(valueFromIndex(index), 'f', prec);
    }
    else if (role == Qt::TextAlignmentRole) {
        res = Qt::AlignCenter;
    }

    return res;
}

Qt::ItemFlags TableModel::flags(const QModelIndex & index) const
{
    Q_UNUSED(index)
    return Qt::ItemIsSelectable |
            Qt::ItemIsEditable |
            Qt::ItemIsEnabled |
            Qt::ItemNeverHasChildren;
}

int TableModel::columnCount(const QModelIndex & parent) const
{
    Q_UNUSED(parent)
    return 4;
}

int TableModel::rowCount(const QModelIndex & parent) const
{
    Q_UNUSED(parent)
    return values.size() / columnCount();
}

double &TableModel::valueFromIndex(const QModelIndex &index)
{
    return values[index.row()*4+index.column()];
}

const double &TableModel::valueFromIndex(const QModelIndex &index) const
{
    return values[index.row()*4+index.column()];
}

bool TableModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (!index.isValid())
        return false;

    if (role == Qt::DisplayRole ||
            role == Qt::EditRole) {

        // si modification de données
        if (index.row() < rowCount() && index.column() < columnCount())
            valueFromIndex(index) = value.toString().toDouble();
        // sinon si ajout (interdit ici)
        else
            return false;

        QVector<int> roles;
        roles << role;
        emit dataChanged(index, index, roles);

        return true;
    }
    return false;
}
