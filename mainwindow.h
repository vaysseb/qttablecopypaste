#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QPushButton>
#include <QMainWindow>
#include <QAbstractItemModel>
#include <QtWidgets/QApplication>


class TableModel;


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

public slots:
    void setEditor(bool enable);

protected:
    void keyPressEvent(QKeyEvent* event);

private slots:
    void closeApp() const { qApp->quit(); }

    void copyTableToClipboard();
    void pasteClipboardToTable();

    void on_actionQuit_triggered() const { closeApp(); }
    void on_actionCopy_triggered() { copyTableToClipboard(); }
    void on_actionPaste_triggered() { pasteClipboardToTable(); }

    void on_buttonQuit_clicked() const { closeApp(); }
    void on_buttonCopy_clicked() { copyTableToClipboard(); }
    void on_buttonPaste_clicked() { pasteClipboardToTable(); }

    void resizeEditorButtonGeometry();

private:
    Ui::MainWindow *ui;

    TableModel* model;

    QPushButton* _editorButton;

    QRect selectedRect() const;
};

//------------------------------------------------------------------------------

class TableModel : public QAbstractTableModel
{
    Q_OBJECT
public:
    explicit TableModel(QObject* parent = NULL);

    QVariant data(const QModelIndex & index, int role = Qt::DisplayRole) const;

    int columnCount(const QModelIndex & parent = QModelIndex()) const;

    int rowCount(const QModelIndex & parent = QModelIndex()) const;

    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const;

    bool setData(const QModelIndex &index, const QVariant &value, int role);

    Qt::ItemFlags flags(const QModelIndex & index) const;

private:
    QList<double> values;

    double &valueFromIndex(const QModelIndex &index);
    const double& valueFromIndex(const QModelIndex &index) const;
};

#endif // MAINWINDOW_H
